package hr.dturic.personal.ferIlluminativeApp.di

import hr.dturic.personal.ferIlluminativeApp.common.database.AppDB
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers

object Provider {
    fun provideScope() = CoroutineScope(CoroutineName("fer_illuminative_app") + Dispatchers.IO)

    fun provideCardsDAO() = AppDB.getDB().cardsDAO()
}