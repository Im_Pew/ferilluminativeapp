package hr.dturic.personal.ferIlluminativeApp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import hr.dturic.personal.ferIlluminativeApp.presentation.features.cards.CardsFragment
import hr.dturic.personal.ferIlluminativeApp.common.Navigation
import hr.dturic.personal.ferIlluminativeApp.common.database.AppDB

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Navigation.root(CardsFragment())
        AppDB.getDB()
    }
}