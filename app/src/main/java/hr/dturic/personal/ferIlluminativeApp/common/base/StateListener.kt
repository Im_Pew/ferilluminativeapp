package hr.dturic.personal.ferIlluminativeApp.common.base

interface StateListener {

    fun onSuccess()
    fun onLoading(show: Boolean)
    fun onError(error: String)
}