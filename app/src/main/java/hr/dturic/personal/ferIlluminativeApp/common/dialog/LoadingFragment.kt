package hr.dturic.personal.ferIlluminativeApp.common.dialog

import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import hr.dturic.personal.ferIlluminativeApp.R

class LoadingFragment : DialogFragment(R.layout.fragment_load) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.Theme_App_Dialog_FullScreen)
    }

    override fun show(manager: FragmentManager, tag: String?) {
        manager.executePendingTransactions()
        if (manager.findFragmentByTag(tag) != null) return
        super.show(manager, tag)
    }
}