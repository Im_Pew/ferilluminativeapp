package hr.dturic.personal.ferIlluminativeApp.data.model

import hr.dturic.personal.ferIlluminativeApp.data.model.CardType.Companion.toModel
import hr.dturic.personal.ferIlluminativeApp.data.model.database.CardDetailsDB

data class CardDetails(
    val cardId: String,
    val cardCode: String,
    val expirationDate: String,
    val cardType: CardType,
    val bankName: String,
    val cardOwner: String,
    val canSend: Boolean,
    val canReceive: Boolean,
    val cardName: String
) {
    companion object {
        fun CardDetailsDB.toModel() = CardDetails(
            cardId = cardId,
            cardCode = cardCode,
            expirationDate = expirationDate,
            cardType = cardType.toModel(),
            bankName = bankName,
            cardOwner = cardOwner,
            canSend = canSend,
            canReceive = canReceive,
            cardName = cardName
        )
    }
}