package hr.dturic.personal.ferIlluminativeApp.data.model.database

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CardDetailsDB(
    @PrimaryKey val cardId: String,
    @ColumnInfo(name = "card_code") val cardCode: String,
    @ColumnInfo(name = "expiration_date") val expirationDate: String,
    @Embedded val cardType: CardTypeDB,
    @ColumnInfo(name = "bank_name") val bankName: String,
    @ColumnInfo(name = "card_owner") val cardOwner: String,
    @ColumnInfo(name = "can_send") val canSend: Boolean,
    @ColumnInfo(name = "can_receive") val canReceive: Boolean,
    @ColumnInfo(name = "card_name") val cardName: String
)
