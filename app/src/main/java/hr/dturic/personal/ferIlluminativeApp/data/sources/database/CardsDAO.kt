package hr.dturic.personal.ferIlluminativeApp.data.sources.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import hr.dturic.personal.ferIlluminativeApp.data.model.database.CardDB
import hr.dturic.personal.ferIlluminativeApp.data.model.database.CardDetailsDB
import hr.dturic.personal.ferIlluminativeApp.data.model.database.CardTypeDB

@Dao
interface CardsDAO {

    // CardDB
    @Query("SELECT * FROM CardDB")
    fun getCards(): List<CardDB>

    @Query("SELECT * FROM CardDB where id=(:id)")
    fun getCard(id: String): CardDB

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updateCards(cards: List<CardDB>)

    @Query("DELETE FROM CardDB WHERE CardDB.id NOT IN (:cardsIds)")
    fun removeOldCards(cardsIds: List<String>)

    // CardTypeDB
    @Query("SELECT * FROM CardTypeDB")
    fun getCardTypes(): List<CardTypeDB>

    @Query("SELECT * FROM CardDetailsDB where cardId=(:id)")
    fun getCardTypes(id: String): CardDetailsDB

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updateCardTypes(cardTypes: List<CardTypeDB>)

    // CardDetailsDB
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updateCardDetails(cardDetails: CardDetailsDB)
}