package hr.dturic.personal.ferIlluminativeApp.presentation.features.cards

import androidx.recyclerview.widget.DiffUtil
import hr.dturic.personal.ferIlluminativeApp.data.model.Card

class CardsListDiffUtil(oldCardsList: List<Card>, newCardsList: List<Card>) : DiffUtil.Callback() {

    private var oldItemsList: List<Card> = oldCardsList
    private var newItemsList: List<Card> = newCardsList


    override fun getOldListSize() = oldItemsList.size

    override fun getNewListSize(): Int {
        return newItemsList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldItemsList[oldItemPosition] == newItemsList[newItemPosition]
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldItemsList[oldItemPosition].cardId == newItemsList[newItemPosition].cardId
    }
}