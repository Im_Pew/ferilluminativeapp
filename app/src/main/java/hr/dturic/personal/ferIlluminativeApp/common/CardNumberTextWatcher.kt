package hr.dturic.personal.ferIlluminativeApp.common

import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher


class CardNumberTextWatcher : TextWatcher {

    /**
     * Formats the watched EditText to a credit card number
     */
    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
    override fun afterTextChanged(s: Editable) {
        // Remove spacing char
        if (s.isNotEmpty() && s.length % LENGTH == 0) {
            val c = s[s.length - 1]
            if (DIVIDER == c) {
                s.delete(s.length - 1, s.length)
            }
        }
        // Insert char where needed.
        if (s.isNotEmpty() && s.length % LENGTH == 0) {
            val c = s[s.length - 1]
            // Only if its a digit where there should be a space we insert a space
            if (s.length >= MAX_CHARS) {
                s.delete(s.length - 1, s.length)
            } else if (Character.isDigit(c) && TextUtils.split(
                    s.toString(),
                    DIVIDER.toString()
                ).size <= 3
            ) {
                s.insert(s.length - 1, DIVIDER.toString())
            }
        }
    }

    companion object {
        // Change this to what you want... ' ', '-' etc..
        private const val MAX_CHARS = 19
        private const val DIVIDER = ' '
        private const val LENGTH = 5
    }
}