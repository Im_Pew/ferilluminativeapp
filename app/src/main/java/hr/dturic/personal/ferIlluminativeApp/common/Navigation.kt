package hr.dturic.personal.ferIlluminativeApp.common

import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

object Navigation {

    private var fragmentManager: FragmentManager? = null
    private var containerId: Int? = null

    private val stack = mutableListOf<Fragment>()

    fun setContainer(@IdRes id: Int) {
        containerId = id
    }

    fun setFragmentManager(fragmentManager: FragmentManager) {
        this.fragmentManager = fragmentManager
    }

    fun root(fragment: Fragment) {
        setFragment(fragment)
        stack.clear()
        stack.add(fragment)
    }

    fun replace(fragment: Fragment) {
        setFragment(fragment)
        if (stack.isNotEmpty()) stack.removeLast()
        stack.add(fragment)
    }

    fun add(fragment: Fragment) {
        setFragment(fragment)
        stack.add(fragment)
    }

    fun popup(): Boolean {
        if (stack.size == 1) return false
        stack.removeLast()
        setFragment(stack.last())
        return true
    }

    private fun setFragment(fragment: Fragment) =
        containerId?.let { fragmentManager?.beginTransaction()?.replace(it, fragment)?.commit() }
}