package hr.dturic.personal.ferIlluminativeApp.presentation.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.widget.AppCompatSpinner
import hr.dturic.personal.ferIlluminativeApp.R
import hr.dturic.personal.ferIlluminativeApp.common.loadImageInto
import hr.dturic.personal.ferIlluminativeApp.data.model.CardType
import kotlinx.android.synthetic.main.item_card_type_drop_down.view.*


class DropdownView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AppCompatSpinner(context, attrs, defStyleAttr),
    AdapterView.OnItemSelectedListener {

    var spinnerItems: List<CardType> = emptyList()

    var onItemsSelectedListener: (CardType) -> Unit = {}

    init {
        this.emptyView =
            LayoutInflater.from(context).inflate(R.layout.item_card_type_selected_item, null)
    }

    fun setItems(cardTypes: List<CardType>) {
        spinnerItems = cardTypes
        this.adapter = CardsDropdownAdapter(cardTypes).also {
            it.setDropDownViewResource(R.layout.item_card_type_selected_item)
        }
        this.invalidate()
        this.onItemSelectedListener = this
    }

    override fun getSelectedItem(): CardType {
        return spinnerItems[selectedItemPosition]
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if (spinnerItems.isEmpty()) return
        onItemsSelectedListener(spinnerItems[position])
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {}

    inner class CardsDropdownAdapter(var items: List<CardType>) :
        ArrayAdapter<CardType>(context, R.layout.item_card_type_selected_item, items) {

        override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
            return getDropDownCardsView(position, convertView, parent)
        }

        private fun getDropDownCardsView(
            position: Int,
            convertView: View?,
            parent: ViewGroup
        ): View {
            val cardType = items[position]
            val view = LayoutInflater.from(context)
                .inflate(R.layout.item_card_type_drop_down, parent, false)

            with(view) {
                cardIcon.loadImageInto(cardType.imageUrl)
                cardName.text = cardType.kind.name
            }
            return view
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            return getCardsView(position, convertView, parent)
        }

        private fun getCardsView(position: Int, convertView: View?, parent: ViewGroup): View {
            val cardType = items[position]
            val view = LayoutInflater.from(context)
                .inflate(R.layout.item_card_type_selected_item, parent, false)

            with(view) {
                cardIcon.loadImageInto(cardType.imageUrl)
                cardName.text = cardType.kind.name
            }
            return view
        }
    }
}