package hr.dturic.personal.ferIlluminativeApp.common.dialog

import android.os.Bundle
import android.view.View
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import hr.dturic.personal.ferIlluminativeApp.R
import hr.dturic.personal.ferIlluminativeApp.common.visibleIf
import kotlinx.android.synthetic.main.dialog_alert.*

class AlertDialog(
    private val message: String? = null,
    private val onButtonListener: () -> Unit = {}
) : DialogFragment(R.layout.dialog_alert) {

    companion object {
        const val TAG: String = "success_dialog"
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        isCancelable = false
        setupUI()
    }

    private fun setupUI() {
        dialogMessage.visibleIf({ message != null }) {
            text = message
        }

        dialogButton.setOnClickListener {
            dismiss()
            onButtonListener()
        }
    }

    fun show(manager: FragmentManager) {
        manager.executePendingTransactions()
        if (manager.findFragmentByTag(TAG) != null) return
        super.show(manager, TAG)
    }
}