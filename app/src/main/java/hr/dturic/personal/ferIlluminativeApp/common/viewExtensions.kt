package hr.dturic.personal.ferIlluminativeApp.common

import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import com.bumptech.glide.Glide
import hr.dturic.personal.ferIlluminativeApp.R

fun View.gone() {
    visibility = View.GONE
}

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun <T : View> T.visibleIf(
    condition: () -> Boolean,
    visibility: Int = View.GONE,
    apply: T.() -> Unit = {}
) {
    if (condition()) {
        visible()
        apply(this)
    } else this.visibility = visibility
}

fun <T : View> T.goneIf(condition: Boolean, apply: T.() -> Unit = {}) {
    if (condition) gone()
    else {
        visible()
        apply()
    }
}

fun AppCompatImageView.loadImageInto(imageUrl: String) =
    Glide.with(this.context)
        .load(imageUrl)
        .placeholder(R.drawable.icaccountbankaplaceholder)
        .into(this)