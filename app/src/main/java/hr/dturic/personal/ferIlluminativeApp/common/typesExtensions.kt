package hr.dturic.personal.ferIlluminativeApp.common

import java.text.SimpleDateFormat
import java.util.*

fun Date.toAPIDate(): String =
    SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(this)