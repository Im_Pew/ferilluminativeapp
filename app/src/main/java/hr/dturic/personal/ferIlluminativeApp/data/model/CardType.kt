package hr.dturic.personal.ferIlluminativeApp.data.model

import hr.dturic.personal.ferIlluminativeApp.data.model.database.CardTypeDB

data class CardType(
    val id: Int,
    val imageUrl: String,
    val _kind: String
) {

    val kind: Type
        get() = Type.valueOf(_kind)

    companion object {
        enum class Type {
            MASTERCARD, VISA, AMERICAN_EXPRESS, DINERS
        }

        fun CardTypeDB.toModel() = CardType(
            id = this.cardTypeId,
            imageUrl = this.imageUrl,
            _kind = this.kind
        )
    }
}