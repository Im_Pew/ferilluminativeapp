package hr.dturic.personal.ferIlluminativeApp.data.model

import hr.dturic.personal.ferIlluminativeApp.data.model.CardType.Companion.toModel
import hr.dturic.personal.ferIlluminativeApp.data.model.database.CardDB


data class Card(
    val cardId: String,
    val name: String,
    val expirationDate: String,
    val cardCode: String,
    val cardType: CardType
) {
    companion object {

        fun CardDB.toModel() = Card(
            cardId = this.id,
            name = this.name,
            expirationDate = this.expirationDate,
            cardCode = this.cardCode,
            cardType = this.cardType.toModel()
        )
    }
}