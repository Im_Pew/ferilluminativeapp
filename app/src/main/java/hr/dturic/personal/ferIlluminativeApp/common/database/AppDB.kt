package hr.dturic.personal.ferIlluminativeApp.common.database

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import hr.dturic.personal.ferIlluminativeApp.App
import hr.dturic.personal.ferIlluminativeApp.BuildConfig
import hr.dturic.personal.ferIlluminativeApp.data.model.database.CardDB
import hr.dturic.personal.ferIlluminativeApp.data.model.database.CardDetailsDB
import hr.dturic.personal.ferIlluminativeApp.data.model.database.CardTypeDB
import hr.dturic.personal.ferIlluminativeApp.data.sources.database.CardsDAO
import java.lang.IllegalStateException

@Database(
    entities = [CardDB::class, CardTypeDB::class, CardDetailsDB::class],
    version = BuildConfig.ROOM_DATABASE_VERSION
)
abstract class AppDB : RoomDatabase() {
    abstract fun cardsDAO(): CardsDAO

    companion object {
        @Volatile
        private var INSTANCE: AppDB? = null

        fun getDB(): AppDB {
            if (INSTANCE == null) {
                synchronized(AppDB::class.java) {
                    val context = App.instance?.applicationContext ?: throw IllegalStateException("App is dead!")
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(
                            context.applicationContext,
                            AppDB::class.java, BuildConfig.ROOM_DATABASE_NAME
                        ).build()
                    }
                }
            }
            return INSTANCE!!
        }
    }
}
