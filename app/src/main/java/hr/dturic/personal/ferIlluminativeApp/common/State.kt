package hr.dturic.personal.ferIlluminativeApp.common

import hr.dturic.personal.ferIlluminativeApp.common.Status.*

enum class Status {
    SUCCESS, ERROR, LOADING
}

data class State<out T>(val status: Status, val data: T?, val message: String?) {
    companion object {
        fun <T> success(data: T?): State<T> {
            return State(SUCCESS, data, null)
        }

        fun <T> error(msg: String, data: T?): State<T> {
            return State(ERROR, data, msg)
        }

        fun <T> loading(data: T?): State<T> {
            return State(LOADING, data, null)
        }
    }
}