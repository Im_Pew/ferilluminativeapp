package hr.dturic.personal.ferIlluminativeApp.presentation.features.cardCreate

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hr.dturic.personal.ferIlluminativeApp.common.State
import hr.dturic.personal.ferIlluminativeApp.di.Provider
import hr.dturic.personal.ferIlluminativeApp.data.model.CardType
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.util.*

class CardAddViewModel : ViewModel() {

    private val coroutineScope: CoroutineScope = Provider.provideScope()

    val cardTypesLiveData = MutableLiveData<State<List<CardType>>>()

    init {
        getCardTypes()
    }

    fun addCard(
        cardCode: String,
        cardExpiryDate: Date,
        cardOwner: String,
        cardBankName: String,
        cardType: Int,
        cardCanSend: Boolean,
        cardCanReceive: Boolean,
        cardName: String
    ) = coroutineScope.launch {
        cardTypesLiveData.postValue(State.loading(null))
        cardTypesLiveData.postValue(State.error("TODO: Potreno je implementirati metodu", null))
    }

    fun getCardTypes() = coroutineScope.launch {
        cardTypesLiveData.postValue(State.loading(null))
        cardTypesLiveData.postValue(State.error("TODO: Potreno je implementirati metodu", null))
    }
}