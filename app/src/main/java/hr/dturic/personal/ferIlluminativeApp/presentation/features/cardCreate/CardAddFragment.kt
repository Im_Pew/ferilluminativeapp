package hr.dturic.personal.ferIlluminativeApp.presentation.features.cardCreate

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import hr.dturic.personal.ferIlluminativeApp.R
import hr.dturic.personal.ferIlluminativeApp.Utils
import hr.dturic.personal.ferIlluminativeApp.common.*
import hr.dturic.personal.ferIlluminativeApp.common.base.BaseFragment
import hr.dturic.personal.ferIlluminativeApp.data.model.CardType
import kotlinx.android.synthetic.main.fragment_card_add.*

class CardAddFragment : BaseFragment(R.layout.fragment_card_add) {

    private val cardAddViewModel: CardAddViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()
        bind()
    }

    private fun setupUI() {
        appBar.onBackButton = { Navigation.popup() }

        cardCreate.setOnClickListener {}

        cardCodeInput.addTextChangedListener(CardNumberTextWatcher())
        cardExpirationDateInput.addTextChangedListener(CardExpiryTextWatcher())
    }

    private fun bind() {
        cardAddViewModel.cardTypesLiveData.observe(viewLifecycleOwner) {
            when (it.status) {
                Status.LOADING -> onLoading(true)
                Status.SUCCESS -> onSuccess(it.data!!)
                Status.ERROR -> onError(it.message ?: "")
            }
        }
    }

    private fun checkFormData(): Boolean {
        var valid: Boolean = true
        cardNameInput.text.let {
            if (it.isEmpty()) {
                cardNameError.text = getString(R.string.label_empty_field_error)
                valid = false
            } else cardNameError.gone()
        }
        cardCodeInput.text.let {
            val cardCode = it.toString().replace(" ", "")
            if (cardCode.isEmpty()) {
                cardCodeError.text = getString(R.string.label_empty_field_error)
                valid = false
            } else if (cardCode.length != 16) {
                cardCodeError.text = getString(R.string.label_card_code_chars_requirement_error)
                valid = false
            } else cardCodeError.gone()
        }
        cardExpirationDateInput.text.let {
            if (it.isNullOrEmpty()) {
                cardExpirationDateError.text = getString(R.string.label_empty_field_error)
                valid = false
            } else if (!it.matches(Utils.expiryDateFormat)) {
                cardExpirationDateError.text =
                    getString(R.string.label_card_expiry_date_format_error)
                valid = false
            } else cardExpirationDateError.gone()
        }
        cardBankNameInput.text.let {
            if (it.isNullOrEmpty()) {
                cardBankNameError.text =
                    getString(R.string.label_empty_field_error)
                valid = false
            } else cardBankNameError.gone()
        }
        cardOwnerNameInput.text.let {
            if (it.isNullOrEmpty()) {
                cardOwnerNameError.text = getString(R.string.label_empty_field_error)
                valid = false
            } else if (!it.matches(Utils.cardOwnerFormat)) {
                cardOwnerNameError.text = getString(R.string.label_card_owner_special_chars_error)
                valid = false
            } else cardOwnerNameError.gone()
        }

        return valid
    }

    private fun onSuccess(cardTypes: List<CardType>) {
        super.onSuccess()
        cardTypeDropdown.setItems(cardTypes)
    }
}