package hr.dturic.personal.ferIlluminativeApp.presentation.features.cards

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import hr.dturic.personal.ferIlluminativeApp.R
import hr.dturic.personal.ferIlluminativeApp.common.Navigation
import hr.dturic.personal.ferIlluminativeApp.common.Status.*
import hr.dturic.personal.ferIlluminativeApp.common.base.BaseFragment
import hr.dturic.personal.ferIlluminativeApp.presentation.features.cardCreate.CardAddFragment
import hr.dturic.personal.ferIlluminativeApp.presentation.features.cardDetails.CardDetailsFragment
import hr.dturic.personal.ferIlluminativeApp.data.model.Card
import kotlinx.android.synthetic.main.fragment_cards.*

class CardsFragment : BaseFragment(R.layout.fragment_cards) {

    companion object {
        const val TAG = "CardsFragment"
    }

    private val viewModel: CardsViewModel by activityViewModels()

    private var cardsAdapter: CardsAdapter = CardsAdapter()

    private val menuItemListener: (MenuItem) -> Boolean = { item ->
        when (item.itemId) {
            R.id.addNewCard -> {
                Navigation.add(CardAddFragment())
                true
            }
            R.id.reverseOrder -> {
                cardsAdapter.reorderItems()
                true
            }
            else -> false
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
        bind()
    }

    private fun setupUI() {
        with(cardsList) {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = cardsAdapter
        }

        refreshCardsList.setOnRefreshListener {
            viewModel.fetchData()
        }

        appBar.menu = R.menu.cards_menu
        appBar.menuItemClickListener = menuItemListener

        cardsAdapter.itemClickListener = {
            Navigation.add(CardDetailsFragment(it.cardId))
        }
    }

    private fun bind() {
        viewModel.cardsLiveData.observe(requireActivity()) {
            when (it.status) {
                LOADING -> onLoading(true)
                SUCCESS -> onSuccess(it.data!!)
                ERROR -> onError(it.message ?: "")
            }
        }
    }

    private fun onSuccess(cards: List<Card>) {
        super.onSuccess()
        refreshCardsList.isRefreshing = false
        cardsAdapter.updateCardsListItems(cards)
    }
}