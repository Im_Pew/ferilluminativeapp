package hr.dturic.personal.ferIlluminativeApp.data.model.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CardTypeDB(
    @PrimaryKey val cardTypeId: Int,
    @ColumnInfo(name = "image_url") val imageUrl: String,
    @ColumnInfo(name = "kind") val kind: String
)