package hr.dturic.personal.ferIlluminativeApp.presentation.features.cards

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import hr.dturic.personal.ferIlluminativeApp.common.State
import hr.dturic.personal.ferIlluminativeApp.di.Provider
import hr.dturic.personal.ferIlluminativeApp.data.model.Card
import hr.dturic.personal.ferIlluminativeApp.data.model.CardDetails
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class CardsViewModel : ViewModel() {

    private val coroutineScope: CoroutineScope = Provider.provideScope()

    val cardsLiveData = MutableLiveData<State<List<Card>>>()
    val cardDetailsLiveData = MutableLiveData<State<CardDetails>>()

    init {
        fetchData()
    }

    fun fetchData() = coroutineScope.launch {
        cardsLiveData.postValue(State.loading(null))
        cardsLiveData.postValue(State.error("TODO: Potreno je implementirati metodu", null))
    }

    fun fetchCardData(cardId: String) = coroutineScope.launch {
        cardDetailsLiveData.postValue(State.loading(null))
        cardDetailsLiveData.postValue(State.error("TODO: Potreno je implementirati metodu", null))
    }
}