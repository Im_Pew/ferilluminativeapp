package hr.dturic.personal.ferIlluminativeApp

import hr.dturic.personal.ferIlluminativeApp.presentation.views.SendReceiveView
import java.text.SimpleDateFormat
import java.util.*

object Utils {
    fun censureCardCode(code: String) =
        code.replaceRange(5, 15, "**** ****")

    fun checkIfCardIsExpired(expirationDate: String): Boolean {
        val formatChecker = Regex("^\\d\\d/\\d\\d\$")

        if (!formatChecker.matches(expirationDate)) return false

        val values = expirationDate.split("/")
        val parsedCurrentDate =
            SimpleDateFormat("MM/yy", Locale.getDefault())
                .format(Date())
                .split("/")

        return values[1].toInt() <= parsedCurrentDate[1].toInt() &&
                values[0].toInt() <= parsedCurrentDate[0].toInt()
    }

    fun prepareSendReceiveStatus(sendReceiveView: SendReceiveView): String {
        return "S karticom možeš: ${sendReceiveView.getStatusAsString()}"
    }

    val dateExpiryFormatter: SimpleDateFormat =
        SimpleDateFormat("MM/yy", Locale.getDefault())
    val expiryDateFormat: Regex = Regex("(0[1-9]|1[0-2])/?([0-9]{4}|[0-9]{2})")
    val cardOwnerFormat: Regex = Regex("([a-zA-ZČĆĐŽŠčćđšž -])*")
}
