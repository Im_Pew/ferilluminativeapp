package hr.dturic.personal.ferIlluminativeApp.presentation.views

import android.app.Activity
import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.MenuItem
import android.view.View
import androidx.annotation.MenuRes
import androidx.constraintlayout.widget.ConstraintLayout
import hr.dturic.personal.ferIlluminativeApp.R
import hr.dturic.personal.ferIlluminativeApp.common.goneIf
import kotlinx.android.synthetic.main.view_app_bar.view.*

class AppBarView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    var onBackButton: () -> Unit = {}

    var isBackButtonHidden: Boolean = false
        set(value) {
            field = value
            backButton.goneIf(value)
        }

    var menuItemClickListener: (MenuItem) -> Boolean = { true }

    var title: String = ""
        set(value) {
            field = value
            appbarTitle.text = value
        }

    @MenuRes
    var menu: Int? = null
        set(value) {
            field = value
            if (value != null) (context as Activity).menuInflater.inflate(value, actionsMenu.menu)
            else actionsMenu.removeAllViews()
        }

    init {
        View.inflate(context, R.layout.view_app_bar, this)

        var ta: TypedArray? = null
        try {
            ta = getContext().obtainStyledAttributes(attrs, R.styleable.AppBarView)
            title = ta.getString(R.styleable.AppBarView_appbarTitle) ?: ""
            isBackButtonHidden = ta.getBoolean(R.styleable.AppBarView_backButtonHidden, false)
        } finally {
            ta?.recycle()
        }

        backButton.setOnClickListener { onBackButton() }
        actionsMenu.setOnMenuItemClickListener {
            menuItemClickListener(it)
        }
    }
}