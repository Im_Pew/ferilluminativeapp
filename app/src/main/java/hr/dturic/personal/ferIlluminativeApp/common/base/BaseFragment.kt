package hr.dturic.personal.ferIlluminativeApp.common.base

import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import hr.dturic.personal.ferIlluminativeApp.common.dialog.LoadingFragment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

open class BaseFragment(@LayoutRes layout: Int) : Fragment(layout), StateListener {

    private val mainScope = CoroutineScope(Dispatchers.Main)

    private val loadingFragment: LoadingFragment by lazy {
        LoadingFragment()
    }

    override fun onSuccess() {
        onLoading(false)
    }

    override fun onLoading(show: Boolean) {
        lifecycleScope.launchWhenCreated {
            if (show) loadingFragment.show(
                requireActivity().supportFragmentManager,
                "loading_fragment"
            )
            else if (loadingFragment.isAdded) loadingFragment.dismiss()
        }
    }

    override fun onError(error: String) {
        mainScope.launch {
            Snackbar.make(requireView(), error, Snackbar.LENGTH_LONG).show()
        }
        onLoading(false)
    }

    protected fun showDebugSnackBar(message: String) {
        mainScope.launch {
            Snackbar.make(requireView(), message, Snackbar.LENGTH_LONG).show()
        }
    }
}