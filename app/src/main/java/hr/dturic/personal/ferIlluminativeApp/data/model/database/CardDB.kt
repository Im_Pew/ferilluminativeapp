package hr.dturic.personal.ferIlluminativeApp.data.model.database

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CardDB(
    @PrimaryKey val id: String,
    @ColumnInfo(name = "card_code") val cardCode: String,
    @ColumnInfo(name = "card_name") val name: String,
    @ColumnInfo(name = "card_expiration_date") val expirationDate: String,
    @Embedded val cardType: CardTypeDB
)