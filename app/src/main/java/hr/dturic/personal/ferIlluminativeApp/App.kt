package hr.dturic.personal.ferIlluminativeApp

import android.app.Activity
import android.app.Application
import android.os.Bundle
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import hr.dturic.personal.ferIlluminativeApp.common.Navigation

class App : Application(), Application.ActivityLifecycleCallbacks {

    companion object {
        var instance: App? = null
    }

    override fun onCreate() {
        super.onCreate()
        registerActivityLifecycleCallbacks(this)
        Navigation.setContainer(android.R.id.content)

        instance = this
    }

    override fun onActivityCreated(activity: Activity, bundle: Bundle?) {
        Navigation.setFragmentManager((activity as AppCompatActivity).supportFragmentManager)
        addBackButtonListener(activity)
    }

    override fun onActivityStarted(activity: Activity) {
        Navigation.setFragmentManager((activity as AppCompatActivity).supportFragmentManager)
        addBackButtonListener(activity)
    }

    override fun onActivityResumed(activity: Activity) {
        Navigation.setFragmentManager((activity as AppCompatActivity).supportFragmentManager)
        addBackButtonListener(activity)
    }

    override fun onActivityPaused(activity: Activity) {}

    override fun onActivityStopped(activity: Activity) {}

    override fun onActivitySaveInstanceState(activity: Activity, bundle: Bundle) {}

    override fun onActivityDestroyed(activity: Activity) {}

    private fun addBackButtonListener(activity: AppCompatActivity) {
        activity.onBackPressedDispatcher.addCallback(object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (!Navigation.popup()) {
                    activity.finish()
                }
            }
        })
    }
}