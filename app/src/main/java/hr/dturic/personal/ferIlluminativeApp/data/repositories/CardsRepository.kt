package hr.dturic.personal.ferIlluminativeApp.data.repositories

import hr.dturic.personal.ferIlluminativeApp.common.ResponseHandler
import hr.dturic.personal.ferIlluminativeApp.common.State
import hr.dturic.personal.ferIlluminativeApp.data.model.Card
import hr.dturic.personal.ferIlluminativeApp.data.model.Card.Companion.toModel
import hr.dturic.personal.ferIlluminativeApp.data.model.CardDetails
import hr.dturic.personal.ferIlluminativeApp.data.model.CardType
import hr.dturic.personal.ferIlluminativeApp.data.model.CardType.Companion.toModel
import hr.dturic.personal.ferIlluminativeApp.data.sources.database.CardsDAO

class CardsRepository(
    private val cardsDAO: CardsDAO
) {

    suspend fun fetchCards(): State<List<Card>> {
        return try {
            ResponseHandler.handleSuccess(cardsDAO.getCards().map { it.toModel() })
        } catch (e: Exception) {
            ResponseHandler.handleException(e)
        }
    }

    suspend fun fetchCardTypes(): State<List<CardType>> {
        return try {
            val cardTypes = cardsDAO.getCardTypes()
            ResponseHandler.handleSuccess(cardTypes.map { it.toModel() })
        } catch (e: Exception) {
            ResponseHandler.handleException(e)
        }
    }

    suspend fun getCardTypes(): State<List<CardType>> {
        return try {
            val cards = cardsDAO.getCardTypes().map { it.toModel() }
            if (cards.isEmpty()) return fetchCardTypes()
            ResponseHandler.handleSuccess(cards)
        } catch (e: Exception) {
            ResponseHandler.handleException(e)
        }
    }

    suspend fun fetchCard(id: String): State<CardDetails> {
        return try {
            ResponseHandler.handleException(RuntimeException("TODO: Implement method"))
        } catch (e: Exception) {
            ResponseHandler.handleException(e)
        }
    }
}