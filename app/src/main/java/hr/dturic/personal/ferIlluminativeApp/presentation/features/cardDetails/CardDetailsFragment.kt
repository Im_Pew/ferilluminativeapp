package hr.dturic.personal.ferIlluminativeApp.presentation.features.cardDetails

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import hr.dturic.personal.ferIlluminativeApp.R
import hr.dturic.personal.ferIlluminativeApp.Utils
import hr.dturic.personal.ferIlluminativeApp.common.Navigation
import hr.dturic.personal.ferIlluminativeApp.common.Status.*
import hr.dturic.personal.ferIlluminativeApp.common.base.BaseFragment
import hr.dturic.personal.ferIlluminativeApp.common.loadImageInto
import hr.dturic.personal.ferIlluminativeApp.presentation.features.cards.CardsViewModel
import hr.dturic.personal.ferIlluminativeApp.data.model.CardDetails
import kotlinx.android.synthetic.main.fragment_card_details.*
import kotlinx.android.synthetic.main.view_app_bar.*

class CardDetailsFragment(private val cardId: String) :
    BaseFragment(R.layout.fragment_card_details) {

    private val viewModel: CardsViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
        bind()
    }

    override fun onStart() {
        super.onStart()
        viewModel.fetchCardData(cardId)
    }

    private fun setupUI() {
        cardSettingButtons.setOnClickListener {
            Toast.makeText(
                requireContext(),
                getString(R.string.label_not_available),
                Toast.LENGTH_SHORT
            ).show()
        }

        appBar.onBackButton = { Navigation.popup() }
    }

    private fun bind() {
        viewModel.cardDetailsLiveData.observe(requireActivity()) {
            when (it.status) {
                SUCCESS -> onSuccess(it.data!!)
                ERROR -> onError(it.message ?: "")
                LOADING -> onLoading(true)
            }
        }
    }

    private fun onSuccess(cardDetails: CardDetails) {
        super.onSuccess()
        onLoading(false)

        lifecycleScope.launchWhenCreated {
            cardLogo.loadImageInto(cardDetails.cardType.imageUrl)
            cardCodeValue.text = cardDetails.cardCode
            appbarTitle.text = cardDetails.cardName
            sendReceiveStatus.set(cardDetails.canReceive, cardDetails.canSend)
            sendReceiveValue.text = sendReceiveStatus.getStatusAsString()

            sendReceiveStatus.setOnClickListener {
                showDebugSnackBar(Utils.prepareSendReceiveStatus(sendReceiveStatus))
            }
        }
    }
}